"""
----------------------------
        Table Unflipper
----------------------------
"""

from discord.ext import commands

class TableUnflipper(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        @bot.event
        async def on_message(message):
            flipped_table = "(╯°□°）╯︵ ┻━┻"
            if(flipped_table in message.content):
                await message.channel.send("Honestly {}!\nI'm always cleaning up after you!\n┬─┬ノ( º _ ºノ)".format(message.author.mention))
            await bot.process_commands(message)

def setup(bot):
    bot.add_cog(TableUnflipper(bot))
