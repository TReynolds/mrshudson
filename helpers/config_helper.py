def getClientID():
    filein = open('config/clientID.txt', 'r')
    clientID = filein.readline().strip()
    filein.close()
    return clientID

def getSecret():
    filein = open('config/secret.txt', 'r')
    TOKEN = filein.readline().strip()
    filein.close()
    return TOKEN
