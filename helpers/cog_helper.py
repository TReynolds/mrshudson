import os

"""
Register the cog for use
"""
def register_cog(cog):
    if(not already_registered(cog)):
        # open file to append
        with open("config/installed_cogs.ini", "a") as file:
            # add cog to file
            file.write(cog+"\n")
        return "{} has now been registered.".format(cog)
    else:
        return "{} has already been registered.".format(cog)

"""
Remove cog from use
"""
def unregister_cog(cog):
    # open file to read
    with open("config/installed_cogs.ini", "r") as file_read:
        #save data for removal
        file_data = file_read.read()
        
    # split data
    file_data = file_data.split('\n')
    # remove value
    file_data.remove(cog)
    # filter array and remove empty/null values
    filtered_array = [x for x in file_data if(not x.isspace() and not x == '')]
    # join back to newline seperated string
    file_data = "\n".join(filtered_array)

    # open file to write new data
    with open("config/installed_cogs.ini", "w") as file_write:
        file_write.write(file_data)

"""
Determine if cog has already been registered
"""
def already_registered(cog):
    # open file as read and return whether cog exists
    with open("config/installed_cogs.ini", "r") as file:
        return True if cog in file.read() else False


"""
Determine if there are pre-existing cogs
"""
def do_cogs_exist():
    # open file to read
    with open("config/installed_cogs.ini", "r") as file:
        file_data = file.read()
    # returns whether file is empty
    return False if (file_data.isspace() or file_data == '') else True


"""
Return a list of
"""
def get_registered_cogs():
    with open("config/installed_cogs.ini", "r") as file:
        file_data = file.read()
        file_array = file_data.split('\n')
        return_string = [x for x in file_array if(not x.isspace() and not x == '')]
    return return_string

"""
Get list of all cogs
"""
def get_all_cogs():
    base_dir = os.getcwd()
    cog_dir = "cogs/"
    all_files = os.listdir(base_dir + "/" + cog_dir)
    formatted_file_names = [x[:-3] for x in all_files if(x not in ["__init__.py", "__pycache__"])]
    return formatted_file_names