# Mrs. Hudson

The housekeeping bot for all (some) of your server needs!



## How Do I Employ Mrs. Hudson For My Server?

Simple, my dear Watson.  

First up, you will require Python, anything 3.6 or higher shall do the trick.

Any package manager such as PIP will do, just as long as you can install either Discord.py or Discord.py[Voice]

Once all the dependencies have been installed, then you can head on over to the developers portal, [found here](https://discordapp.com/developers/applications/), to create a bot account for Mrs. Hudson.

After creation, you will need to note down the ClientID and the secret TOKEN.  These will be used later.

Clone this repo somewhere onto your machine/server

You will need to create two files, clientID.txt and secret.txt inside the config folder, and populate them with your ClientID and TOKEN respectively.

And that should be it.

All that's required now is to run the bot.

`python3 bot.py`





## Authors Notes:

I'm too lazy to write a proper guide.  If someone wants to write out a fully fledged guide then sure.

Still need to implement some example cogs for reference and would be nice to have as default.