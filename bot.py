import discord

from discord.ext import commands

"""
----------------------------
        Custom Imports
----------------------------
"""
from helpers import config_helper
from helpers import cog_helper

print('Turning on...\nPlease Standbye\n')

bot = commands.Bot(command_prefix='!')


"""
------------------------
        On Ready
------------------------
"""
@bot.event
async def on_ready():
    print('Checking for registered cogs...')
    if(cog_helper.do_cogs_exist()):
        print('Cogs found...\n')
        cogs_list = cog_helper.get_registered_cogs()
        for x in cogs_list:
            print("Loading {}...".format(x))
            bot.load_extension("cogs."+x)
            print("{} Loaded.\n".format(x))
    else:
        print('No cogs found.')
        print('')

    print('Mrs. Hudson is online and ready...')
    print('-'*50)
    print('Invite me into your server:\n\nhttps://discordapp.com/oath2/authorize?client_id={}&scope=bot'.format(config_helper.getClientID()))


"""
--------------------------------
        Default commands
--------------------------------
"""
@bot.command(brief="Outputs a list of all available cogs", pass_context=True)
async def list_cogs(ctx):
    all_cogs = cog_helper.get_all_cogs()
    cog_list_string = "```\nList of all available cogs:"
    for name in all_cogs:
        cog_list_string += "\n\t -- {}".format(name)
    cog_list_string += "```"
    await ctx.channel.send(cog_list_string)

@bot.command(brief="Load an available cog", pass_context=True)
async def load_cog(ctx):
    all_cogs = cog_helper.get_all_cogs()
    message = ctx.message.content
    message = message.replace(bot.command_prefix+'load_cog', '')
    message = message.strip()
    if(message in all_cogs):
       cog_loading = await ctx.channel.send("Loading Cog...")
       if cog_helper.already_registered(message):
           await cog_loading.edit(content="Loading Cog...\n{} already registered...\nCancelling...".format(message))
       else:
           await cog_loading.edit(content="Loading Cog...\nPreparing to load {}...".format(message))
           bot.load_extension("cogs."+message)
           await cog_loading.edit(content="Loading Cog...\nPreparing to load {}...\n{}".format(message, cog_helper.register_cog(message)))

@bot.command(brief="List all installed cogs", pass_context=True)
async def list_installed_cogs(ctx):
    installed_cogs_array = cog_helper.get_registered_cogs()
    installed_string = "```List of all installed cogs:"
    for x in installed_cogs_array:
        installed_string += "\n\t-- {}".format(x)
    installed_string += "```"
    await ctx.channel.send(installed_string)

@bot.command(brief="Unloads a loaded cog", pass_context=True)
async def unload_cog(ctx):
    message = ctx.message.content
    message = message.replace(bot.command_prefix+'unload_cog', '')
    message = message.strip()
    
    unload_string = await ctx.channel.send("Unloading Cog...")
    if(cog_helper.do_cogs_exist()):
        if(message in cog_helper.get_registered_cogs()):
            await unload_string.edit(content="Unloading Cog...\nInstalled cog {} found...".format(message))
            cog_helper.unregister_cog(message)
            await unload_string.edit(content="Unloading Cog...\nInstalled cog {} found...\n{} has now been unregistered...\n".format(message, message))
            bot.remove_cog(message)
            await unload_string.edit(content="Unloading Cog...\nInstalled cog {} found...\n{} has now been unregistered...\n{} has now been removed.".format(message, message, message))
        else:
            await unload_string.edit(content="Unloading Cog...\n{} is not registered.\nCancelling".format(message))
    else:
        await unload_string.edit(content="Unloading Cog...\nNo installed cogs to remove")


"""
--------------------------------
        Register the bot
--------------------------------
"""
bot.run(config_helper.getSecret())
